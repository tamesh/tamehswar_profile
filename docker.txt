Docker Setup
Download Docker Community Edition
https://store.docker.com/editions/community/docker-ce-desktop-mac
Create Sign up
Install Docker
After install sign in with the docker id
To check the docker is working or not : docker -v

Run this command to install the docker image
docker run -it -u root -d -p 8080:8080 -p 50000:50000 -v jenkins-data:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock jenkinsci/blueocean
docker ps
docker ps -a
Enter Password
Ge the password from this command docker exec -it 44d07caaa9ab /bin/bash

To See the logs
docker logs


For extra settings
https://jenkins.io/doc/book/installing/#downloading-and-running-jenkins-in-docker