const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = [
    { test: /\.(js|jsx)$/, loader: 'react-hot-loader!babel-loader', exclude: [path.resolve(__dirname, '/node_modules/')] },
    { test: /\.scss$/, loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader!resolve-url-loader!sass-loader' }) },
    { test: /\.css$/, loader: 'style-loader!resolve-url-loader!css-loader' },
    { test: /\.(png|jpe?g|gif)$/i, loader: 'url-loader?limit=1000&name=./src/assets/images/[name].[hash:8].[ext]'},
    { test: /\.(eot|svg|ttf|woff|woff2)$/, loader: "file-loader?name=./src/assets/fonts/[name].[ext]"},
];