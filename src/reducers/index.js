import { combineReducers } from 'redux';
import skillsReducer from './skills';
const rootReducer = combineReducers({
    skillState: skillsReducer
});

export default rootReducer;
