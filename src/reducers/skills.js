import { LIST_OF_SKILLS, ADD_SKILLS } from '../components/Skills/constant';

const INITIAL_STATE = {
    skills: [],
    loading: true
};

const applySetters = (state, action) => ({
    ...state,
    skills: action.skills
});

function skillsReducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case LIST_OF_SKILLS: {
            return applySetters(state, action);
        }
        case ADD_SKILLS: {
            return Object.assign({}, state, {skills: state.skills.concat(action.data.skills) });
        }
        default:
            return state;
    }
}



export default skillsReducer;