export const LANDING_PAGE = '/';
export const HOME = '/home';
export const ABOUT_US = '/about-us';
export const PORTFOLIO = '/portfolio';
export const CONTACT_US = '/contact-us';
export const SKILLS = '/skills';
export const WORKS_AND_PLAY = '/works-and-play';
