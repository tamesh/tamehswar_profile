const Loading = () => (
  <div className="preloader">
    <div className="loader">
        <div className="loader__figure"></div>
    </div>
  </div>
);

const LoaderHOC = (propName) => (WrappedComponent) => {
  return class LoaderHOC extends React.Component {
    isEmpty(prop) {
      return ( prop === null  || prop === undefined  || (Array.isArray(prop) && prop.length === 0)  || (prop.constructor === Object && Object.keys(prop).length === 0) );
    }
    render() {
      return ( this.isEmpty(this.props[propName])  ? <Loading /> : <WrappedComponent {...this.props} /> )
    }
  }
}

export {
  LoaderHOC
};