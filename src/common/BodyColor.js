import { Component } from 'react';
import PropTypes from 'prop-types';

class BodyColor extends Component {

    static defaultProps = {
        bgColorCls: "skin-yellow-dark"
    }

    componentDidMount() {
        document.body.setAttribute('class', this.props.bgColorCls);
    }

    componentWillMount() {
        document.body.removeAttribute('class', '');
    }
    render() {
        return this.props.children
    }
}

BodyColor.propTypes = {
    bgColor: PropTypes.string
}

// BodyColor.defaultProps = {
//     bgColor: "#ffbc34"
// }

export default BodyColor;