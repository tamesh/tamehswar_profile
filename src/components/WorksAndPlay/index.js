import React, { Component } from 'react';
import { LoaderHOC } from '../../common/loading-spiner';
import BodyColor from '../../common/BodyColor';

class WorksAndPlayPage extends Component {
  componentDidMount() {
    
  }

  render() {
   
    return (
      <BodyColor bgColorCls="works-n-play">
        <LoadingWorksAndPlay loading={true} />
      </BodyColor>
    );
  }
}

const WorksAndPlay = () => (
  <div>
    <h3 className="w3l-head"><span className="title-head">W</span>orks and play</h3>
    <h4 className="pb-1" style={{borderBottom:'1px dashed #ccc'}}>Domain</h4>
    <div className="row mb-3">
        <div className="col-6 pb-2">
            <i className="mdi mdi-hospital-building mr-1"></i>
            Healthcare
        </div>
        <div className="col-6 pb-2">
            <i className="mdi mdi-deskphone mr-1" />
            Retail
        </div>
        <div className="col-6 pb-2">
            <i className="mdi mdi-account-card-details mr-1" />
            Banking and Finance
        </div>
        <div className="col-6 pb-2">
            <i className="mdi mdi-satellite-variant mr-1" />
            Telecom
        </div>
    </div>

    <div className="row">
                            
        <div className="col-sm-12 col-md-6 mb-3" style={{borderBottom:'1px dashed #ccc'}}>
            
            <div className="">
                <div className="card-body">
                    <div className="bpi"> </div>
                    {/* <h4 className="card-title">Bank of Philippine</h4> */}
                    <p className="card-text">Environment: Nodejs, Angular JS, JavaScript, Bootstrap, Webpack, HTML5, CSS3, SCSS, REST, JSON</p>
                </div>
            </div>
            
        </div>

        <div className="col-sm-12 col-md-6 mb-3" style={{borderBottom:'1px dashed #ccc'}}>
            
            <div className="">
                <div className="card-body">
                    <div className="autodesk"> </div>
                    {/* <h4 className="card-title">Autodesk</h4> */}
                    <p className="card-text">Environment: Nodejs, Angular JS, JavaScript, SCSS, REST, JSON</p>
                </div>
            </div>
            
        </div>
        
        
        <div className="col-sm-12 col-md-6 mb-3" style={{borderBottom:'1px dashed #ccc'}}>
            
            <div className="">
                <div className="card-body">
                    {/* <h4 className="card-title">Globant</h4> */}
                    <div className="globant"> </div>
                    <p className="card-text">
                        Environment: HTML5, CSS3, ReactJs, Redux, ExtJs 3.4, Ember js, JavaScript, JSON, AJAX, jQuery, Underscore, Bootstrap, REST, SCSS
                    </p>
                </div>
            </div>
            
        </div>

        <div className="col-sm-12 col-md-6 mb-3" style={{borderBottom:'1px dashed #ccc'}}>
            <div className="">
                <div className="card-body">
                    {/* <h4 className="card-title">Mastercard</h4> */}
                    <div className="mastercard"> </div>
                    <p className="card-text">Environment: Nodejs, Angular JS, ReactJs, Redux, JavaScript, Lodash, Bootstrap, Webpack, HTML5, CSS3, SCSS, REST, JSON</p>
                </div>
            </div>
        </div>
        
    </div>

  </div>
);


const LoadingWorksAndPlay = LoaderHOC('loading')(WorksAndPlay);

export default WorksAndPlayPage;
