import React from 'react';
import { NavLink } from 'react-router-dom';
import * as routes from '../../constants/routes';

const Navigation = () => <NavigationAuth />;

const NavigationAuth = () => (
  <header className="topbar">
  <nav className="navbar top-navbar navbar-expand-md navbar-dark">
  <div className="">
    <ul className="nav">
      <li className="nav-item">
        <NavLink replace exact to={routes.LANDING_PAGE} className="nav-link" activeClassName="active-cls">
          <div>
            <em className="mdi mdi-home"></em>
          </div>
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink replace exact to={routes.HOME} className="nav-link" activeClassName="active-cls">
          <div>
            <em className="mdi mdi-account-circle mr-1"></em>
            About Me
          </div>
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink replace exact to={routes.SKILLS} className="nav-link" activeClassName="active-cls">
          <div>
            <em className="mdi mdi-lightbulb-on mr-1"></em>
            Skills
          </div>
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink replace exact to={routes.WORKS_AND_PLAY} className="nav-link" activeClassName="active-cls">
          <div>
            <em className="mdi mdi-projector mr-1"></em>
            Works and Play
          </div>
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink replace exact to={routes.CONTACT_US} className="nav-link" activeClassName="active-cls">
          <div>
            <em className="mdi mdi-contact-mail mr-1"></em>
            Contact Us
          </div>
        </NavLink>
      </li>
    </ul>
    </div>

  </nav>
  </header>
);

export default Navigation;
