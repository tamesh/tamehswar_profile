import React, { Component } from 'react';
import { LoaderHOC } from '../../common/loading-spiner';
import BodyColor from '../../common/BodyColor';

const ContactUs = () => (
    <div>
        <h3 className="w3l-head"><span className="title-head">C</span>ontact us</h3>
        <div>
            <h2>Let's Keep In Touch!</h2>
            <h4>Thank you for visiting out my profile. If you would like to get into contact with me.</h4>
            <div>
                <a href="https://www.linkedin.com/in/tameshwar-nirmalkar-a073011b/" target="_blank"> 
                    <i className="mdi mdi-linkedin-box text-info fa-2x" /> 
                </a>
                <a href="https://github.com/TameshwarNirmalkar" target="_blank">
                    <i className="mdi mdi-github-box text-muted fa-2x " />
                </a>
                <a href="https://www.facebook.com/tameshwar" target="_blank">
                    <i className="mdi mdi-facebook-box text-primary fa-2x " />
                </a>
                <a href="https://plus.google.com/+tameshwarnirmalkartest" target="_blank">
                    <i className="mdi mdi-google-plus-box text-danger fa-2x " />
                </a>
            </div>
        </div>
    </div>
)
class ContactUsPage extends Component {
    componentDidMount() {
      
    }
  
    render() {
     
      return (
        <BodyColor bgColorCls="contact-us">
            <LoadingContactUs loading={true} />
        </BodyColor>
      );
    }
  }

const LoadingContactUs = LoaderHOC('loading')(ContactUs);


export default ContactUsPage;