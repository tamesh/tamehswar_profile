import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import BodyColor from '../../common/BodyColor';

const LandingComponent = () => (
  <BodyColor bgColorCls="landing-body">
    <div className="landing-page-description">
      <div>
        {/* <div className="baner-bubble">Hello!</div> */}
        <img style={{visibility:'visible'}} src="https://firebasestorage.googleapis.com/v0/b/tameshwar-nirmalkar.appspot.com/o/singlePic.png?alt=media&token=9dcaa08c-7d3c-4fb3-b261-23f104ec229f" alt="Tameshwar Nirmalkar" className="rounded-circle border border-white" width="100" height="100"/>
        <h1>Tameshwar Nirmalkar</h1>
        {/* <div>Principal Developer</div> */}
        <div className="mt-3">
          <NavLink replace exact to="/home">
            <button className="btn btn-bg-color text-white rounded-0">View Profile</button>
          </NavLink>
          <a target="_blank" className="rounded-0 ml-3 btn btn-bg-color text-white"
            href="https://firebasestorage.googleapis.com/v0/b/tameshwar-nirmalkar.appspot.com/o/Latest_Resume.pdf?alt=media&token=57ce7356-8cd2-4eeb-a905-52c03f5b2b36">
            Resume <i className="fa fa-download hire-icon" aria-hidden="true"></i>
          </a>
        </div>
      </div>
    </div>
  </BodyColor>);

class LandingPage extends Component {
  componentDidMount() {
    
  }

  render() {
    return <LandingComponent />
  }
}

export default LandingPage;
