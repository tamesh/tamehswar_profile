import React, { Component } from 'react';
import { LoaderHOC } from '../../common/loading-spiner';
import BodyColor from '../../common/BodyColor';
import { Progress } from 'reactstrap';

class HomePage extends Component {
  componentDidMount() {
  }

  render() {
   
    return (
      <BodyColor bgColorCls="home-about-us">
        <LoadingHomePageContent loading={true} />
      </BodyColor>
    );
  }
}

const HomePageContent = () => (
  <div className="home-page">
    <h3 className="w3l-head"><span className="title-head">A</span>bout me</h3>
    <div className="w-100"></div>
    <div className="container">
      <div className="row">
        <div className="col education">
            <div className="skills">
              <h3 className="w3l_head"><i className="fa fa-shield wthree-title-list" aria-hidden="true"></i>expertise</h3>
              <div className="skill-agile">
                <div className="bar_group group_ident-1">
                  <p className="b_label">JS</p>
                  <Progress className="" value="90" />
                  <p className="b_label">Angular Js</p>
                  <Progress className="" value="80" />
                  <p className="b_label">React Js</p>
                  <Progress className="" value="80" />
                  <p className="b_label">Redux/Flux</p>
                  <Progress className="" value="70" />
                  <p className="b_label">Node Js</p>
                  <Progress className="" value="90" />
                  <p className="b_label">Webpack</p>
                  <Progress className="" value="70" />
                </div>
                
              </div>
            </div>

            <div className="education-w3l">
              <h3 className="w3l_head three"><i className="fa fa-graduation-cap  wthree-title-list" aria-hidden="true"></i>Education</h3>
              <div className="education-agile-w3l">
                <div className="education-agile-w3l-year">
                  <h4 className="pull-left"><i className="fa fa-caret-right  wthree-title-list" aria-hidden="true"></i>
                    B.C.A (AI)
                  </h4>
                  
                  <div className="clearfix"></div>
                </div>
                <div className="education-agile-w3l-info">
                    <p>Bacheclor of Computer Application in Artificial Intelligence</p>
                </div>
              </div>
            </div>
        </div>
        <div className="col-sm-12 col-md-12 col-lg-8 experience">
          <div className="skills">
            <h3 className="w3l_head"><i className="fa fa-file-text-o wthree-title-list"></i>Profile</h3>
            <div className="agileits-skill-agile">
            <ul>
              <li>Working as Technical Lead with around 12 years of experience on front-end JavaScript based web development.</li>
              <li>Hands on experience in Nodejs, ReactJs, Angular JS, Angular Material, HTML5, CSS3, Bootstrap, SCSS, JavaScript, AJAX, and jQuery.</li>
              <li>Knowledge of database concepts of SQL such as PostgreSQL, MySql.</li>
              <li>Worked on projects on agile methodology.</li>
              <li>Attended Adobe UX Trainings and Usability Workshops to Bring UX into Consideraton when Developing Web Applicatons.</li>
            </ul>
            </div>
          </div>
          <div className="education-w3l">
            <h3 className="w3l_head three"><i className="fa fa-briefcase  wthree-title-list" aria-hidden="true"></i>Experience</h3>
            <div className="education-agile-grids">
              <div className="education-agile-w3l">
                <div className="education-agile-w3l-year">
                  <h4 className="pull-left"><i className="fa fa-caret-right  wthree-title-list" aria-hidden="true"></i>
                    Mobiquity (Principal Developer)
                  </h4>
                  <h4 className="pull-right">2017-Present</h4>
                  <div className="clearfix"></div>
                </div>
                <div className="education-agile-w3l-info">
                    <p>Work in Progress</p>
                </div>
              </div>
              <div className="education-agile-w3l">
                <div className="education-agile-w3l-year">
                  <h4 className="pull-left"><i className="fa fa-caret-right  wthree-title-list" aria-hidden="true"></i>
                    Altimetrik (Technical Lead)
                  </h4>
                  <h4 className="pull-right">2016-2017</h4>
                  <div className="clearfix"></div>
                </div>
                <div className="education-agile-w3l-info">
                  <p>Work in Progress</p>
                </div>
              </div>
              <div className="education-agile-w3l">
                <div className="education-agile-w3l-year">
                  <h4 className="pull-left"><i className="fa fa-caret-right  wthree-title-list" aria-hidden="true"></i>
                    Globant (Technical Lead)
                  </h4>
                  <h4 className="pull-right">2015-2016</h4>
                  <div className="clearfix"></div>
                </div>
                <div className="education-agile-w3l-info">
                  <p>Work in Progress</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);


const LoadingHomePageContent = LoaderHOC('loading')(HomePageContent);

export default HomePage;
