import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import { TransitionGroup, CSSTransition } from "react-transition-group";

// import Loadable from 'react-loadable';

// import { Loading } from '../../common/loading-spiner';
import { MasterLayout } from './master-layout';
import HomePage from '../Home';
import ContactUs from '../ContactUs';
import LandingPage from '../Landing';
import SkillPage from '../Skills';
import WorksAndPlay from '../WorksAndPlay';

// import withAuthentication from '../Session/withAuthentication';
import * as routes from '../../constants/routes';

const App = () =>
  <Router>
    <Route render={({ location }) => console.log( location) || (
      <div className="main-body-container">
          
        <Switch>
          <Route exact path={routes.LANDING_PAGE} component={() => <LandingPage />} />
            <MasterLayout>
              <TransitionGroup>
                <CSSTransition timeout={300} key={location.key} classNames="fade">
                  <Switch>
                    <Route exact path={routes.HOME} component={() => <HomePage />} />
                    <Route exact path={routes.CONTACT_US} component={() => <ContactUs />} />
                    <Route exact path={routes.SKILLS} component={() => <SkillPage />} />
                    <Route exact path={routes.WORKS_AND_PLAY} component={() => <WorksAndPlay />} />
                  </Switch>
                </CSSTransition>
              </TransitionGroup>
            </MasterLayout>
        </Switch>

      </div>
    )}>
    </Route>
  </Router>

export default App;