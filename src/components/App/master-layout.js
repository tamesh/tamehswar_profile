import Navigation from '../Navigation';

const MasterLayout = ({children}) => (
    <div id="main-wrapper">
      <Navigation />

      {/* Left Side Bar */}
      
      {/* Main Page Container */}
      <div className="mt-4">
        <div className="container-fluid">
          { children }
        </div>
      </div>
    </div>
  );

export {
    MasterLayout
};