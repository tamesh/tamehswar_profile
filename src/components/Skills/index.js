import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { LoaderHOC } from '../../common/loading-spiner';
import BodyColor from '../../common/BodyColor';
import { Progress, Form, FormGroup, Label, Button, Input, Card, CardTitle, CardBody } from 'reactstrap';
import { skillsApi } from '../../firebase';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import * as axios from 'axios';
import { LIST_OF_SKILLS, ADD_SKILLS } from './constant';
import { updateByPropertyName, guid } from '../../common/utiliti-helper';

const INITIAL_STATE = { skills:null, skillsLabel: '', skillsRating:'', loading: true };

class SkillPage extends Component {

    constructor(props) {
        super(props);
        this.state = { ...INITIAL_STATE };
    }
    componentDidMount() {
        const { onSetSkills } = this.props;
        axios.get('https://us-central1-tameshwar-nirmalkar.cloudfunctions.net/webApi/api/skills')
            .then(function (response) {
                onSetSkills(response.data.skills);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    onSubmit = (e) => {
        // const id = guid();
        const { onAddSkills } = this.props;
        const paylodSkills = this.state;
        delete paylodSkills.skills;
        const paylod = {skills: paylodSkills};
        axios.post('https://us-central1-tameshwar-nirmalkar.cloudfunctions.net/webApi/api/skills', paylod)
            .then((response) => {
                onAddSkills({skills: paylodSkills, loading: this.state.loading});
            })
            .catch((error) => {
                
            });
        e.preventDefault();
    }

    render() {
        const { skillsLabel, skillsRating, loading } = this.state;
        return (
            <BodyColor bgColorCls="skills-page">
                <h3 className="w3l-head"><span className="title-head">P</span>rograming skills</h3>
                <div className="row col-sm-12 col-md-12 col-lg-6" style={{display:'none'}}>
                    <Form className="col-12" onSubmit={this.onSubmit}>
                        <FormGroup>
                            <Label>Skills</Label>
                            <Input type="text" value={skillsLabel} placeholder="Skills Name: eg. Angular, React, Node, Java"
                            onChange={event => this.setState(updateByPropertyName('skillsLabel', event.target.value))} />
                        </FormGroup>
                        <FormGroup>
                            <Label>Rating</Label>
                            <Input type="text" pattern="\d{0,2}?%?" value={skillsRating} placeholder="Skills Rating: eg. 60%, 80%" 
                            onChange={event => this.setState(updateByPropertyName('skillsRating', event.target.value))}/>
                        </FormGroup>
                        <Button color="primary" type="submit" disabled={loading}>
                            <em className="fa fa-refresh fa-spin" style={{ display: (loading ? 'none':'inline-block')}} />
                            Add Skills
                        </Button>
                    </Form>
                </div>
                <LoadingSkillsPageComponent list={this.props.skills} />
            </BodyColor>
        );
    }
}

const setColorByValue = (colorValue) => {
    if(colorValue > 1 && colorValue < 40){
        return 'danger';
    }
    else if(colorValue > 40 && colorValue < 60){
        return 'warning';
    }
    else if(colorValue > 60 && colorValue < 100){
        return 'succes';
    }
    else {
        return 'info';
    }
}

const SkillsPageComponent = ({list}) => {
    return (
        <div className="">
            <div className="row">
                <div className="col-sm-12 col-md-6 col-lg-6">
                {
                    list.map((item, key) => (
                        <div className="mb-4" key={key}>
                            <span>{item.skillsLabel}</span>
                            <Progress className="rounded-0" color={setColorByValue(item.skillsRating)} value={item.skillsRating} title={`Progress Value ${item.skillsRating}`}>{item.skillsRating/10}/10</Progress>
                        </div>
                    ))
                }
                </div>
            </div>
        </div>
)};


const LoadingSkillsPageComponent = LoaderHOC('list')(SkillsPageComponent);

const mapStateToProps = (state) => ({
    skills: state.skillState.skills,
});

const mapDispatchToProps = (dispatch) => ({
    onSetSkills: (skills) => dispatch({type: LIST_OF_SKILLS, skills}),
    onAddSkills: (data) => dispatch({type: ADD_SKILLS, data})
})

export default compose(connect(mapStateToProps, mapDispatchToProps))(withRouter(SkillPage));
