import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/functions';

// const config = process.env.NODE_ENV === 'production' ? prodConfig : devConfig;
var config = {
  apiKey: "AIzaSyDEv-ey7APhN29crTyicllBotse63eQBus",
  authDomain: "tameshwar-nirmalkar.firebaseapp.com",
  databaseURL: "https://tameshwar-nirmalkar.firebaseio.com",
  projectId: "tameshwar-nirmalkar",
  storageBucket: "tameshwar-nirmalkar.appspot.com",
  messagingSenderId: "553756570362"
};

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

const db = firebase.database();
const auth = firebase.auth();
const functions = firebase.functions();

export {
  db,
  auth,
  functions,
};
