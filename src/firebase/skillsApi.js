import { db, functions } from './firebase';
export const getSkillsList = functions.httpsCallable('getSkillsList');
export const addSkills = functions.httpsCallable('addSkills');