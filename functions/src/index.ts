import { https } from 'firebase-functions';
import * as express from 'express';
import * as cors from 'cors';
import * as bodyParser from "body-parser";
export * from "./initialize";
import  skillsRouter from "./skills";

const app = express();

// this allows to make reqeusts to same origin.
app.use(cors({ origin: true }));

// Parsing the every request. like form, body.data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// https://expressjs.com/en/advanced/best-practice-security.html#at-a-minimum-disable-x-powered-by-header
app.disable("x-powered-by");

// Any requests to /api/skills will be routed to the skills router!
app.use("/api/skills", skillsRouter);

// Again, lets be nice and help the poor wandering servers, any requests to /api
// that are not /api/users will result in 404.
app.get("*", (req: express.Request, res: express.Response) => {
	res.status(200).send("This route does not exist.");
});

// why this? 
function AppMiddleware(req,res) {
    if (!req.url) {
      req.url = '/';
      req.path = '/';
    }
    return app(req,res);
  }

export const webApi = https.onRequest(app);