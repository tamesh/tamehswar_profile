import * as express from 'express';
import { filterObjects } from './utilityFunction';
import { db } from './db';
const skillsRouter = express.Router();

skillsRouter.get("/", async function getSkillsList(req: express.Request, res: express.Response) {
    const snapshot = await db.ref('skills').once('value').then(snp => {
        if(snp.val()){ 
            return { skills: filterObjects(snp.val()) } 
        }else{
            return { skills: null };
        }
    });
    res.status(200).json(snapshot).end();
});

skillsRouter.post("/", async function setSkills(req: express.Request, res: express.Response) {
    const skills = req.body.skills;
    if (!skills) {
        res.status(404).json({erroCode: 400, errorMessage: 'Payload is not defined'}).end();
    }
    else {
        db.ref('skills').push(skills).then(() => { res.status(204).end(); });
    }    
});

skillsRouter.get("/:uid", async function getSkillsById(req: express.Request, res: express.Response) {
    const uid = req.params.uid;
    if(!uid){
        res.status(404).json({errorCode: 400, errorMessage: "id is undefined, null or empty"}).end();
    }else{
        const snapVal = await db.ref('skills').child(uid).once('value').then((snp) => { 
            return snp.val();
        });
        if(!snapVal){
            res.status(404).json({errorCode: 404, errorMessage: "No Records Found"}).end();
        }
        else{
            res.status(200).json(snapVal).end();
        }
    }
    
});

skillsRouter.delete("/:uid", async function deleteSkillsById(req: express.Request, res: express.Response) {
    const uid = req.params.uid;
    if(!uid){
        res.status(404).json({errorCode: 400, errorMessage: "id is undefined, null or empty"}).end();
    }else{
        await db.ref('skills').child(uid).remove().then(() => { 
            res.status(204).end();
        });
    }
    
});

skillsRouter.patch("/", async function deleteSkillsById(req: express.Request, res: express.Response) {
    const data = req.body.skills;
    if(!data){
        res.status(400).json({errorCode: 400, errorMessage: "data is undefined, null or empty"}).end();
    }else{
        await db.ref('skills').child(data.uid).update(data).then((snp) => { 
            res.status(204).end();
        });
    }
});


skillsRouter.get("*", async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    const err = new Error('Not Found');
    err["status"] = 404;
    next(err);
});

export default skillsRouter;


















// const app = express();
// app.use(cors({ origin: true }));
// app.use('/api/v1', app);
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: false }));
// export const webApi = https.onRequest(app);
















/*
const addData = (req: any, res: any) => {
    res.setHeader('Content-Type', 'application/json');
    console.log('\n\nRequest body: ', req, '\n\n');
    // res.set('Access-Control-Allow-Methods', 'OPTIONS, POST');
    // res.set('Access-Control-Allow-Origin', '*');
    
    // if(!req.query.skills){
    //     res.status(404);
    //     res.end(JSON.stringify({errorCode: 404, errorMessage: "Bad Request"}));
    // }
    // db.ref('skills').set(req.query.skills);
    // res.status(200).send({message: 'data inserted'});
    res.end();
}
export const addSkills = https.onRequest(addData);

const GetSkills = async (req, res) => {
    const snapshot = await db.ref('skills').once('value').then(snp => {
        return { 
            data: { skills:  filterObjects(snp.val()) }
        };
    });
    res.set('Cache-Control', 'public, max-age=300, s-maxage=600');
    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Methods', 'GET, HEAD, OPTIONS, POST, PUT, PATCH, DELETE');
    res.status(200).send(snapshot);
    res.end();
};
export const getSkillsList = https.onRequest(GetSkills);

*/