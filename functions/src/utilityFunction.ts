const filterObjects = (data: Object) => {
    return Object.keys(data).map((key, ind) => Object.assign({}, data[key], {uid: Object.keys(data)[ind]}));
}

export {
    filterObjects
}